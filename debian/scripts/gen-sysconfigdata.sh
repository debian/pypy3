#!/bin/sh
set -eufx

# Writes output to a directory recorded in pybuilddir.txt
pypy/goal/pypy3.11-c -m sysconfig --generate-posix-vars \
	HOST_GNU_TYPE ${DEB_HOST_GNU_TYPE} \
	WHEEL_PKG_DIR /usr/share/python-wheels/ \
	CONFINCLUDEPY /usr/include/pypy3.11 \
	INCLUDEPY /usr/include/pypy3.11 \
	LIBDIR /usr/lib/${DEB_HOST_MULTIARCH} \
	TZPATH /usr/share/zoneinfo:/usr/lib/zoneinfo:/usr/share/lib/zoneinfo:/etc/zoneinfo

builddir=$(cat pybuilddir.txt)
if grep reproducible-path $builddir/_sysconfigdata__${DEB_HOST_MULTIARCH}.py; then
	echo "Build path found in _sysconfigdata"
	exit 1
fi
mv $builddir/_sysconfigdata__${DEB_HOST_MULTIARCH}.py lib_pypy/
