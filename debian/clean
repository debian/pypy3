#!/bin/sh
set -eu

# Temporary build directories
rm -rf build build-*

# Caches
rm -rf rpython/_cache .cache

# Vendored libraries
rm -rf pypy/module/pyexpat/src/

# Build artifacts
find . -name '*.pyc'
find . -name __pycache__ | xargs rm -rf
echo pypy/goal/pypy3.11-c pypy/goal/libpypy3.11-c.so
echo pypy/goal/pycparser pycparser
echo rpython/rlib/rvmprof/src/shared/libbacktrace/config.h
find rpython/translator/c pypy/module/cpyext/test -name '*.o'
find include '(' -name '*.h' -o -name '*.inl' ')'
find lib_pypy '(' -name '*.so' -o -name '*.o' ')'
find lib_pypy -name '*.c' \
             -a ! -name '_ctypes_test.c' \
             -a ! -name '_testcapimodule.c' \
             -a ! -name '_testmultiphase.c' \
             -a ! -path 'lib_pypy/_blake2/*' \
             -a ! -path 'lib_pypy/_cffi_ssl/_cffi_src/*' \
             -a ! -path 'lib_pypy/_libmpdec/*' \
             -a ! -path 'lib_pypy/_sha3/*'
echo lib_pypy/_blake2/_blake2b_cffi.c
echo lib_pypy/_blake2/_blake2s_cffi.c
echo lib_pypy/_sha3/_sha3_cffi.c
echo lib_pypy/_sysconfigdata_*.py
echo pybuilddir.txt
echo pypy/goal/lextab.py
echo pypy/goal/yacctab.py
echo pypy/lextab.py
echo pypy/yacctab.py
echo lextab.py
echo yacctab.py

# Python 2.7
echo cpython27/python

# Tests
echo pypy/test.db
find extra_tests/ -name '*.o'
find lib-python/*/lib2to3/ -name '*Grammar*.pickle'
find lib-python/*/test/data/ -type f -a ! -name README

# Failures
echo core
echo pypy/core
rm -rf .cache

# Docs
rm -rf pypy/doc/_build

find pypy/doc/config -name 'commandline*.rst' \
	-o -name 'objspace*.rst' \
	-o -name 'translation*.rst'
find pypy/doc/config \( \
	-name 'objspace*.txt' \
	-o -name 'translation*.txt' \
	\) -size 0 \
	! -name 'objspace.honor__builtins__.txt' \
	! -name 'objspace.std.sharesmallstr.txt' \
	! -name 'objspace.usemodules._vmprof.txt' \
	! -name 'translation.backendopt.really_remove_asserts.txt' \
	! -name 'translation.icon.txt' \
	! -name 'translation.noprofopt.txt'

# Generated in override_dh_installdeb
echo debian/pypy3.preinst debian/pypy3.postinst
