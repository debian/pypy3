pypy3 (7.3.11+dfsg-2) UNRELEASED; urgency=medium

  This release declares the pypy3 interpreter as EXTERNALLY-MANAGED,
  instructing pip to disallow package installation outside virtualenvs.
  python3-pip (>= 23.0) understands and enforces this.

  See: https://peps.python.org/pep-0668/

  Practically, this means that you can't use pip to install packages outside a
  virtualenv, on Debian's PyPy interpreter by default, any more.

  You can override this behaviour by passing --break-system-packages to pip
  install, but be aware that if you are running pip as root, doing so can
  break your system.

  See /usr/share/doc/python3.11/README.venv for more details.

 -- Stefano Rivera <stefanor@debian.org>  Sun, 05 Feb 2023 18:28:23 -0400
