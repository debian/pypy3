#!/bin/sh

cp -a debian/tests/packages "$AUTOPKGTEST_TMP"

HOME="$AUTOPKGTEST_TMP/home"
PY3_VER=$(pypy3 -c 'import sys; print("{}.{}".format(*sys.version_info[:2]))')
mkdir "$HOME"
site_packages="$HOME/.local/lib/pypy$PY3_VER/site-packages"

tearDown() {
	# Remove the --user install directory
	rm -rf $site_packages
}

testFibPy() {
	cd "$AUTOPKGTEST_TMP/packages/fibpy"
	pypy3 setup.py install --user
	assertTrue 'Install fibpy in --user' $?
	assertTrue 'fibpy was installed to ~/.local' "[ -e $site_packages/fibpy-*.egg ]"
	cd "$AUTOPKGTEST_TMP"
	stdout=$(pypy3 -m fibpy 5)
	assertTrue 'Execute fibpy from ~/.local' $?
	assertEquals 'Correct result' 8 "$stdout"
}

testFibC() {
	cd "$AUTOPKGTEST_TMP/packages/fibc"
	pypy3 setup.py install --user
	assertTrue 'Install fibc in --user' $?
	assertTrue 'fibc was installed to ~/.local' "[ -e $site_packages/fibc-*.egg ]"
	cd "$AUTOPKGTEST_TMP"
	stdout=$(pypy3 -c 'from fibc import fib; print(fib(5))')
	assertTrue 'Execute fibc from ~/.local' $?
	assertEquals 'Correct result' 8 "$stdout"
}

testFibCFFI() {
	cd "$AUTOPKGTEST_TMP/packages/fibcffi"
	pypy3 setup.py install --user
	assertTrue 'Install fibcffi in --user' $?
	assertTrue 'fibcffi was installed to ~/.local' "[ -e $site_packages/fibcffi-*.egg ]"
	cd "$AUTOPKGTEST_TMP"
	stdout=$(pypy3 -m fibcffi 5)
	assertTrue 'Execute fibcffi from ~/.local' $?
	assertEquals 'Correct result' 8 "$stdout"
}

testFibPyDevelop() {
	cd "$AUTOPKGTEST_TMP/packages/fibpy"
	pypy3 setup.py develop --user
	assertTrue 'Install editable fibpy in --user' $?
	assertTrue 'fibpy was linked to ~/.local' "[ -e $site_packages/fibpy.egg-link ]"
	cd "$AUTOPKGTEST_TMP"
	stdout=$(pypy3 -m fibpy 5)
	assertTrue 'Execute editable fibpy from ~/.local' $?
	assertEquals 'Correct result' 8 "$stdout"
}

testFibPyPip() {
	cd "$AUTOPKGTEST_TMP"
	pypy3 -m pip install --no-build-isolation --break-system-packages --user \
		-v "$AUTOPKGTEST_TMP/packages/fibpy"
	assertTrue 'Install fibpy via pip in --user' $?
	assertTrue 'fibpy was installed to ~/.local' "[ -e $site_packages/fibpy.py ]"
	stdout=$(pypy3 -m fibpy 5)
	assertTrue 'Execute fibpy from ~/.local' $?
	assertEquals 'Correct result' 8 "$stdout"
}

testFibPyPipEditable() {
	cd "$AUTOPKGTEST_TMP"
	pypy3 -m pip install --no-build-isolation --user --break-system-packages \
		-v -e "$AUTOPKGTEST_TMP/packages/fibpy"
	assertTrue 'Install editable fibpy via pip in --user' $?
	assertTrue 'fibpy was linked to ~/.local' "[ -e $site_packages/__editable__.fibpy-42.0.0.pth ]"
	stdout=$(pypy3 -m fibpy 5)
	assertTrue 'Execute fibpy from ~/.local' $?
	assertEquals 'Correct result' 8 "$stdout"
}

testFibPyDistutilsLocal() {
	cd "$AUTOPKGTEST_TMP/packages/fibpy"
	SETUPTOOLS_USE_DISTUTILS=local pypy3 setup.py install --user
	assertTrue 'Install fibpy in --user' $?
	assertTrue 'fibpy was installed to ~/.local' "[ -e $site_packages/fibpy-*.egg ]"
	cd "$AUTOPKGTEST_TMP"
	stdout=$(pypy3 -m fibpy 5)
	assertTrue 'Execute fibpy from ~/.local' $?
	assertEquals 'Correct result' 8 "$stdout"
}

. shunit2
